-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for hannaninajla
CREATE DATABASE IF NOT EXISTS `hannaninajla` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hannaninajla`;

-- Dumping structure for table hannaninajla.user
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(5) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `pass_word` varchar(20) NOT NULL,
  `registration_date` datetime NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Dumping data for table hannaninajla.user: ~9 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `email`, `pass_word`, `registration_date`) VALUES
	(1, 'John', 'Hacker', 'john@gmail.com', 'abc123', '2021-06-10 17:20:40'),
	(2, 'Amy', 'Scammer', 'amy @gmail.com', '123456', '2021-06-10 17:22:59'),
	(11, 'Syarifah', 'Zarifa', 'zarifa@gmail.com', 'sz123', '2021-06-10 09:24:40'),
	(12, 'Siti ', 'Suraya', 'suraya@gmail.com', 'ss123', '2021-06-10 09:24:40'),
	(13, 'Nurul ', 'Falahiyah', 'falahiyah@gmail.com', 'nf123', '2021-06-10 09:24:40'),
	(14, 'Puteri ', 'Serah', 'serah@gmail.com', 'ps123', '2021-06-10 09:24:40'),
	(15, 'Atikah ', 'Safira', 'safira@gmail.com', 'as123', '2021-06-10 09:24:40'),
	(16, 'Syed', 'Atiq', 'atiq@gmail.com', 'sa123', '2021-06-10 09:24:40'),
	(17, 'Mohd', 'Qarim', 'qarim@gmail.com', 'mq123', '2021-06-10 09:24:40');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;